package br.ufsc.lapesd.sspsp;

import com.google.gson.Gson;
import dados.criminais.sp.boletins.api.model.*;
import org.apache.commons.io.FileUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class App {
    private final String searchTemplate = "https://www.google.com.br/search?hl=pt-BR&q=%1$s";

    @Option(name = "--flatDir", required = true)
    private File flatDir;

    @Option(name = "--amostraDir", required = true)
    private File amostraDir;

    public static void main(String[] args) throws Exception {
        App app = new App();
        CmdLineParser parser = new CmdLineParser(app);
        parser.parseArgument(args);
        app.run();
    }

    private URI getSearchURI(String query) throws URISyntaxException {
        try {
            return new URI(String.format(searchTemplate, URLEncoder.encode(query, "UTF-8")));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private void run() throws Exception {
        Amostra amostra = loadAmostra(amostraDir);
        batchSearch(amostra);

    }

    private Amostra loadAmostra(File dir) throws IOException {
        try (FileInputStream inputStream = new FileInputStream(new File(dir, "index.json"));
             InputStreamReader reader = new InputStreamReader(inputStream, "UTF-8")) {
            return new Gson().fromJson(reader, Amostra.class);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private BoletimOcorrencia loadBoletim(String id) throws IOException {
        try (FileInputStream stream = new FileInputStream(new File(flatDir, id + ".json"));
             InputStreamReader reader = new InputStreamReader(stream, "UTF-8")) {
            return new Gson().fromJson(reader, BoletimOcorrencia.class);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }


    }

    public boolean batchSearch(Amostra amostra) throws IOException, URISyntaxException {
        File searchDir = new File(amostraDir, "searches");
        if (!searchDir.exists() && !searchDir.mkdirs())
            throw new IOException("Couldn't mkdir " + searchDir.getPath());
        ExpansoesGoogle expansoes;
        try {
            expansoes = loadExpansoes(searchDir);
        } catch (FileNotFoundException e) {
            expansoes = new ExpansoesGoogle();
        }

        for (Amostra.Elemento elemento : amostra.getElementos()) {
            if (expansoes.containsBoletim(elemento.getIdBO())) continue;
            BoletimOcorrencia boletim = loadBoletim(elemento.getIdBO());
            String query = getQueryString(boletim);
            if (query != null) {
                ExpansaoGoogle expansao = null;
                try {
                    expansao = findExpansao(expansoes, query);
                    if (expansao == null)
                        expansao = search(searchDir, query);
                } catch (GoogleHtmlInvalid googleHtmlInvalid) {
                    System.err.printf("Bad Google html for query %1$s\n", query);
                }
                expansoes.addElemento(new ExpansoesGoogle.Elemento(boletim.getIdBO(), expansao));
                saveExpansoes(searchDir, expansoes);
            }
        }
        saveExpansoes(searchDir, expansoes);
        return true;
    }

    private ExpansaoGoogle findExpansao(ExpansoesGoogle expansoes, String query) {
        for (ExpansoesGoogle.Elemento elemento : expansoes.getElementos()) {
            ExpansaoGoogle candidate = elemento.getExpansaoGoogle();
            if (candidate != null && candidate.getId().equals(query))
                return candidate;
        }
        return null;
    }

    private ExpansoesGoogle loadExpansoes(File searchDir) throws IOException {
        try (FileInputStream stream = new FileInputStream(new File(searchDir, "index.json"));
             InputStreamReader reader = new InputStreamReader(stream, "UTF-8")) {
            return new Gson().fromJson(reader, ExpansoesGoogle.class);
        }
    }

    private void saveExpansoes(File searchDir, ExpansoesGoogle expansoes) throws IOException {
        try (FileOutputStream stream = new FileOutputStream(new File(searchDir, "index.json"));
             OutputStreamWriter writer = new OutputStreamWriter(stream, "UTF-8")) {
            new Gson().toJson(expansoes, writer);
        }
    }

    public ExpansaoGoogle search(File dir, String query) throws IOException, URISyntaxException, GoogleHtmlInvalid {
        File searchDir = new File(dir, query);
        if (searchDir.exists()) {
            FileUtils.deleteQuietly(searchDir);
        }
        if (!searchDir.exists() && !searchDir.mkdir()) {
            throw new IOException("Dir " + searchDir.getPath()
                    + " already existed or couldn't be mkdir");
        }

        return doSearch(searchDir, query);
    }

    private ExpansaoGoogle doSearch(File searchDir, String query) throws IOException,
            URISyntaxException, GoogleHtmlInvalid {
        File htmlFile = new File(searchDir, "index.html");

        System.out.printf("Searching for \"%1$s\"\n", query);
        Document document = Jsoup.connect(getSearchURI(query).toString())
                .userAgent("Mozilla/5.0 (X11; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0")
                .header("Accept-Language", "en-US,en;q=0.5")
                .header("Cache-Control", "max-age=0")
                .get();
        FileOutputStream outputStream = new FileOutputStream(htmlFile);
        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
        writer.write(document.toString());

        try {
            return processResults(searchDir, htmlFile, query);
        } catch (GoogleHtmlInvalid e) {
            Files.move(searchDir.toPath(),
                    new File(searchDir.toPath().toString() + ".error").toPath());
            throw e;
        }
    }

    private ExpansaoGoogle processResults(File searchDir, File googleFile, String expansaoId)
            throws GoogleHtmlInvalid, IOException {
        Document doc = Jsoup.parse(googleFile, "UTF-8");
        List<String> urls = doc.getElementsByClass("r").stream().limit(5)
                .map(r -> r.getElementsByTag("a").stream().findFirst())
                .filter(Optional::isPresent).map(Optional::get)
                .map(a -> a.attr("href")).filter(s -> !s.isEmpty())
                .map(this::extractUrl).filter(s -> !s.isEmpty()).collect(Collectors.toList());
        if (urls.isEmpty())
            throw new GoogleHtmlInvalid("No links extracted");

        ExpansaoGoogle expansaoGoogle = new ExpansaoGoogle(expansaoId);

        for (int i = 0; i < urls.size(); i++) {
            String url = urls.get(i);
            for (int j = 0; j < 3; j++) {
                try {
                    if (j > 0)
                        System.out.printf("Retrying fetch on %1$s\n", url);
                    File resultFile = new File(searchDir, (i + 1) + ".html");
                    fetch(url, resultFile);
                    expansaoGoogle.addResultado(
                            new ExpansaoGoogle.Resultado(url, resultFile.getName()));
                    break;
                } catch (HttpStatusException | UnsupportedMimeTypeException e) {
                    e.printStackTrace();
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException ignored) {
                    }
                }
            }
        }
        return expansaoGoogle;
    }

    private void fetch(String url, File file) throws IOException {
        System.out.printf("Fetching %1$s\n", url);
        Document document = Jsoup.connect(url).userAgent("Mozilla/5.0 (X11; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0").get();
        try (FileOutputStream stream = new FileOutputStream(file);
             OutputStreamWriter writer = new OutputStreamWriter(stream, "UTF-8")) {
            writer.write(document.toString());
        }
    }

    private String extractUrl(String googleResultsUrl) {
        final Pattern p1 = Pattern.compile("^/url\\?q=([^&]*)&.*$");
        Matcher matcher = p1.matcher(googleResultsUrl);
        if (matcher.matches())
            return matcher.group(1);

        final Pattern p2 = Pattern.compile("^https?://.*$");
        matcher = p2.matcher(googleResultsUrl);
        if (matcher.matches()) {
            try {
                new URI(googleResultsUrl);
            } catch (URISyntaxException ignored) {
            }
            return googleResultsUrl;
        }

        return "";
    }


    private String getQueryString(BoletimOcorrencia boletim) {
        String query = "";
        for (ParteEnvolvida parte : boletim.getPartesEnvolvidas()) {
            if (parte.getNome() == null || parte.getNome().isEmpty()) continue;
            query += parte.getNome() + " ";
        }
        if (query.isEmpty()) return null;
        query += categoriaAmigavel(boletim.getCategoria());
        return query;
    }

    private String categoriaAmigavel(String categoria) {
        switch (categoria) {
            case "HOMICIDIO_DOLOSO":
                return "homicídio";
            case "LATROCINIO":
                return "latrocínio";
            case "LESAO_CORPORAL_SEGUIDA_DE_MORTE":
                return "óbito";
            case "OPOSICAO_POLICIAL":
                return "confronto homicídio";
            case "SUICIDIO_DUVIDOSO":
                return "suicídio";
            case "CADAVER_SEM_LESOES":
                return "cadáver";
            case "MORTE_ACIDENTAL":
                return "acidente";
            case "MORTE_SUBITA_NATURAL":
                return "morte súbita";
        }
        return "";
    }
}
