package br.ufsc.lapesd.sspsp;

public class GoogleHtmlInvalid extends Exception {
    public GoogleHtmlInvalid(String s) {
        super(s);
    }

    public GoogleHtmlInvalid(String s, Throwable throwable) {
        super(s, throwable);
    }
}
